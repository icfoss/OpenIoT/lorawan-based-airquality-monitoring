# LoRaWAN Air Quality Node

By   implementing   an   air   pollution   monitoring   solution   comprised   of   sensors   andgateways embedded with LoRa Technology and an intelligent low power wide areanetwork based on the LoRaWAN protocol, cities can better measure quality and providethe type of data necessary to drive change for their citizens.

The system is designed using a sharp dust sensor (GP2Y1014AU0F), Humidity-temperature sensor (DHT11). Sharp's GP2Y1014AU0F is an optical air quality sensor, ormay also known as optical dust sensor, is designed to sense dust particles in mg/m3. TheDHT11 is a basic, ultra low-cost digital temperature and humidity sensor. After processingby the micro controller unit (MCU) in the electronic module, the data are transmitted usingLoRaWAN. Here the micro controlled used is ULP LoRa 2.1.

# Getting Started


- Make sure that you have a ULP LoRa Board.

- Install project library from here . Copy the library to ~/Arduino/libraries/

- Select board : Arduino Pro Mini 3.3v 8Mhz



# Prerequisites

Arduino IDE - 1.8.9 (Tested)

**License**
This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/SruthyS/lorawan_airquality/-/blob/master/LICENSE) file for details

**Acknowledgments**

LMIC by [matthijskooijman](https://github.com/matthijskooijman/arduino-lmic)
